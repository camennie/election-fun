This application explores the relationship between profession and likelihood of 
being elected in Canadian elections.

Relationship can be broken down across a range of years, by province, and for 
elected professions versus professions that have never been elected (for top 
10 professions).

Data is shown as a pie chart as well as a chord graph with mouse over tooltips 
for the outside ring groups, as well as the inner individual chords.

Application uses a static database for simplicity, but could be easily extended 
to pull data dynamically.



