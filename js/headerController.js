/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./graphView.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />

var Controllers;
(function (Controllers) {
    var HeaderController = (function () {
        function HeaderController(UIModel) {
            this.UIModel = UIModel;
            bindAll(this);

            this.renderingPie = renderingPie;
        }
        return HeaderController;
    })();
    Controllers.HeaderController = HeaderController;
})(Controllers || (Controllers = {}));

angular.module('ngMainApp').controller('HeaderController', ['ProfessionsUIModel', Controllers.HeaderController]);
//# sourceMappingURL=headerController.js.map
