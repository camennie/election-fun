/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />


enum eProvince {
    Alberta,
    BritishColumbia,
    Manitoba,
    NewBrunswick,
    NewfoundlandAndLabrador,
    NorthwestTerritories,
    NovaScotia,
    Nunavut,
    Ontario,
    PrinceEdwardIsland,
    Quebec,
    Saskatchewan,
    Yukon
}//eProvince


interface IElectionResult {
    Year: number;
    Province: eProvince;
    Occupation: string;
    Elected: boolean;
}//IElectionResult

declare var db: IElectionResult[];


module GraphModels {

    export class ProfessionCountPair {
        profession: string;
        count: number;
    }//ProfessionCountPair

    export class YearProfessionCountPair {
        profession: string;
        count: number;
        year: number;
    }//ProfessionCountPair


    export class GraphModel {
        public topElected: ProfessionCountPair[];
        public topElectedPerYear: YearProfessionCountPair[];
        public yearToProfessionMapping: boolean;
        public useElected: boolean; //as opposed to unelected


        constructor(private UIModel: UIModel.ProfessionsUIModel) {
            bindAll(this);
        }//constructor


        public updateGraphModel() {
            this.yearToProfessionMapping = this.UIModel.yearToProfessionMapping;
            this.useElected = this.UIModel.useElected;

            //Get db entries that match our current filter
            var filteredDb = db.filter((elem: IElectionResult) => {
                var yearOk: boolean = (elem.Year >= this.UIModel.yearRangeStart) && (elem.Year <= this.UIModel.yearRangeEnd);

                var electedOk = elem.Elected == this.UIModel.useElected;

                var provinceOk: boolean = false;
                switch (this.UIModel.provincesSelection.value) {
                    case UIModel.eProvinceSelection.CanadaCombined: provinceOk = true; break;
                    case UIModel.eProvinceSelection.Alberta: provinceOk = elem.Province === eProvince.Alberta; break;
                    case UIModel.eProvinceSelection.BritishColumbia: provinceOk = elem.Province === eProvince.BritishColumbia; break;
                    case UIModel.eProvinceSelection.Manitoba: provinceOk = elem.Province === eProvince.Manitoba; break;
                    case UIModel.eProvinceSelection.NewBrunswick: provinceOk = elem.Province === eProvince.NewBrunswick; break;
                    case UIModel.eProvinceSelection.NewfoundlandAndLabrador: provinceOk = elem.Province === eProvince.NewfoundlandAndLabrador; break;
                    case UIModel.eProvinceSelection.NorthwestTerritories: provinceOk = elem.Province === eProvince.NorthwestTerritories; break;
                    case UIModel.eProvinceSelection.NovaScotia: provinceOk = elem.Province === eProvince.NovaScotia; break;
                    case UIModel.eProvinceSelection.Nunavut: provinceOk = elem.Province === eProvince.Nunavut; break;
                    case UIModel.eProvinceSelection.Ontario: provinceOk = elem.Province === eProvince.Ontario; break;
                    case UIModel.eProvinceSelection.PrinceEdwardIsland: provinceOk = elem.Province === eProvince.PrinceEdwardIsland; break;
                    case UIModel.eProvinceSelection.Quebec: provinceOk = elem.Province === eProvince.Quebec; break;
                    case UIModel.eProvinceSelection.Saskatchewan: provinceOk = elem.Province === eProvince.Saskatchewan; break;
                    case UIModel.eProvinceSelection.Yukon: provinceOk = elem.Province === eProvince.Yukon; break;
                }//switch

                return yearOk && electedOk && provinceOk;
            });//forEach


            //Save a bit of time and compute only what we need to display
            if (!this.yearToProfessionMapping) {
                this.computeTopElected(filteredDb);
            } else {
                this.computeTopElectedPerYear(filteredDb);
            }//if
        }//updateGraphModel


        private computeTopElectedPerYear(filteredDb: IElectionResult[]) {
            var self = this;
            this.topElectedPerYear = [];

            this.computeTopElected(filteredDb);

            var topProfessions: { [key: string]: boolean } = {};
            this.topElected.forEach((elem: ProfessionCountPair) => {
                topProfessions[elem.profession.toLowerCase()] = true;
            });//forEach

            var yearGroups = _.groupBy(filteredDb, (elem) => { return elem.Year; });

            Object.keys(yearGroups).forEach((yearStr: string) => {
                var ers: IElectionResult[] = yearGroups[yearStr];

                ers = ers.filter((er: IElectionResult) => {
                    return er.Occupation.toLowerCase() in topProfessions;
                })//filter

                if (ers.length > 0) {
                    var year = parseInt(yearStr);
                    var pcpsForYear = this.getProfessionCountPairs(ers);

                    var ypcps: YearProfessionCountPair[] = pcpsForYear.map((pcp: ProfessionCountPair) => {
                        return {
                            profession: pcp.profession,
                            count: pcp.count,
                            year: year
                        }
                    });//map

                    ypcps = ypcps.slice(0, 10);
                    self.topElectedPerYear = self.topElectedPerYear.concat(ypcps);
                }//if
            })//forEach
        }//computeTopElectedPerYear


        private computeTopElected(filteredDb: IElectionResult[]) {
            var pcps = this.getProfessionCountPairs(filteredDb);
            this.topElected = pcps.slice(0, 10);
        }//computeTopElected


        private getProfessionCountPairs(filteredDb: IElectionResult[]): ProfessionCountPair[] {
            var professionCountMap: {[key: string]: number} = {};
            filteredDb.forEach((elem: IElectionResult) => {
                if (!(elem.Occupation in professionCountMap)) professionCountMap[elem.Occupation] = 0;
                professionCountMap[elem.Occupation]++;
            });//forEach

            var electedPairs: ProfessionCountPair[] = [];
            for (var profession in professionCountMap) {
                var pcp = new ProfessionCountPair();
                pcp.profession = profession;
                pcp.count = professionCountMap[profession];

                electedPairs.push(pcp);
            }//for

            electedPairs.sort((pcp1: ProfessionCountPair, pcp2: ProfessionCountPair): number => { return pcp2.count - pcp1.count; });
            return electedPairs;
        }//computeTopElected

    }//GraphModel
}//models


angular.module('ngMainApp')
       .service('ProfessionsGraphModel', ['ProfessionsUIModel', GraphModels.GraphModel]);
