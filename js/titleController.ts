/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./graphView.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />


module Controllers {
    export class TitleController {
        constructor(private UIModel: UIModel.ProfessionsUIModel) {
            bindAll(this);

        }//constructor

    }//TitleController
}//Controllers


angular.module('ngMainApp')
    .controller('TitleController', ['ProfessionsUIModel', Controllers.TitleController])
