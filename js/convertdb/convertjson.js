var eProvince;
(function (eProvince) {
    eProvince[eProvince["Alberta"] = 0] = "Alberta";
    eProvince[eProvince["BritishColumbia"] = 1] = "BritishColumbia";
    eProvince[eProvince["Manitoba"] = 2] = "Manitoba";
    eProvince[eProvince["NewBrunswick"] = 3] = "NewBrunswick";
    eProvince[eProvince["NewfoundlandAndLabrador"] = 4] = "NewfoundlandAndLabrador";
    eProvince[eProvince["NorthwestTerritories"] = 5] = "NorthwestTerritories";
    eProvince[eProvince["NovaScotia"] = 6] = "NovaScotia";
    eProvince[eProvince["Nunavut"] = 7] = "Nunavut";
    eProvince[eProvince["Ontario"] = 8] = "Ontario";
    eProvince[eProvince["PrinceEdwardIsland"] = 9] = "PrinceEdwardIsland";
    eProvince[eProvince["Quebec"] = 10] = "Quebec";
    eProvince[eProvince["Saskatchewan"] = 11] = "Saskatchewan";
    eProvince[eProvince["Yukon"] = 12] = "Yukon";
})(eProvince || (eProvince = {}));

var ElectionResult = (function () {
    function ElectionResult() {
    }
    return ElectionResult;
})();

var fs = require('fs');
var file = __dirname + '/dbbase.json';

fs.readFile(file, 'utf8', function (err, data) {
    if (err) {
        console.log('Error: ' + err);
        return;
    }

    dbBase = JSON.parse(data);

    dbBase = dbBase.filter(function (elemBase) {
        return elemBase.Occupation.length > 0;
    });

    var db = dbBase.map(function (elemBase) {
        var elem = new ElectionResult();

        elem.Elected = elemBase.Elected === "1";
        elem.Year = parseInt(elemBase.Date.substring(0, 4));
        elem.Occupation = elemBase.Occupation.toLowerCase();

        if (elemBase.Province === "New Brunswick")
            elem.Province = 3 /* NewBrunswick */;
        else if (elemBase.Province === "Nova Scotia")
            elem.Province = 6 /* NovaScotia */;
        else if (elemBase.Province === "Ontario")
            elem.Province = 8 /* Ontario */;
        else if (elemBase.Province === "Quebec")
            elem.Province = 10 /* Quebec */;
        else if (elemBase.Province === "Manitoba")
            elem.Province = 2 /* Manitoba */;
        else if (elemBase.Province === "British Columbia")
            elem.Province = 1 /* BritishColumbia */;
        else if (elemBase.Province === "Prince Edward Island")
            elem.Province = 9 /* PrinceEdwardIsland */;
        else if (elemBase.Province === "Northwest Territories")
            elem.Province = 5 /* NorthwestTerritories */;
        else if (elemBase.Province === "Yukon")
            elem.Province = 12 /* Yukon */;
        else if (elemBase.Province === "Alberta")
            elem.Province = 0 /* Alberta */;
        else if (elemBase.Province === "Saskatchewan")
            elem.Province = 11 /* Saskatchewan */;
        else if (elemBase.Province === "Newfoundland and Labrador")
            elem.Province = 4 /* NewfoundlandAndLabrador */;
        else if (elemBase.Province === "Nunavut")
            elem.Province = 7 /* Nunavut */;
        else
            console.log("UNKNOWN: " + elemBase.Province);

        return elem;
    });

    //Get hash of elected professions
    var electedProfessions = {};
    db.forEach(function (elem) {
        if (elem.Elected)
            electedProfessions[elem.Occupation.toLowerCase()] = true;
    }); //forEach

    //Remove unelected listings of professions that at one point were elected
    db = db.filter(function (elem) {
        if (elem.Elected)
            return true;

        return !(elem.Occupation.toLowerCase() in electedProfessions);
    });

    console.log("var db = " + JSON.stringify(db, null, 4) + ";");
});
//# sourceMappingURL=convertjson.js.map
