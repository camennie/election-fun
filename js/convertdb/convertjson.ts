enum eProvince {
    Alberta,
    BritishColumbia,
    Manitoba,
    NewBrunswick,
    NewfoundlandAndLabrador,
    NorthwestTerritories,
    NovaScotia,
    Nunavut,
    Ontario,
    PrinceEdwardIsland,
    Quebec,
    Saskatchewan,
    Yukon
}//eProvince


interface IElectionResultBase {
    Date: string;
    Province: string;
    Occupation: string;
    Elected: string;
}//IElectionResultBase


class ElectionResult {
    Year: number;
    Province: eProvince;
    Occupation: string;
    Elected: boolean;
}//ElectionResult


declare var dbBase: IElectionResultBase[];

declare var require: any;
declare var __dirname: any;


var fs = require('fs');
var file = __dirname + '/dbbase.json';

fs.readFile(file, 'utf8', function (err: any, data: any) {
    if (err) {
        console.log('Error: ' + err);
        return;
    }

    dbBase = JSON.parse(data);

    dbBase = dbBase.filter((elemBase:IElectionResultBase) => {
        return elemBase.Occupation.length > 0;
    });

    var db: ElectionResult[] = dbBase.map((elemBase: IElectionResultBase) => {
        var elem = new ElectionResult();

        elem.Elected = elemBase.Elected === "1";
        elem.Year = parseInt(elemBase.Date.substring(0, 4));
        elem.Occupation = elemBase.Occupation.toLowerCase();

        if (elemBase.Province === "New Brunswick") elem.Province = eProvince.NewBrunswick;
        else if (elemBase.Province === "Nova Scotia") elem.Province = eProvince.NovaScotia;
        else if (elemBase.Province === "Ontario") elem.Province = eProvince.Ontario;
        else if (elemBase.Province === "Quebec") elem.Province = eProvince.Quebec;
        else if (elemBase.Province === "Manitoba") elem.Province = eProvince.Manitoba;
        else if (elemBase.Province === "British Columbia") elem.Province = eProvince.BritishColumbia;
        else if (elemBase.Province === "Prince Edward Island") elem.Province = eProvince.PrinceEdwardIsland;
        else if (elemBase.Province === "Northwest Territories") elem.Province = eProvince.NorthwestTerritories;
        else if (elemBase.Province === "Yukon") elem.Province = eProvince.Yukon;
        else if (elemBase.Province === "Alberta") elem.Province = eProvince.Alberta;
        else if (elemBase.Province === "Saskatchewan") elem.Province = eProvince.Saskatchewan;
        else if (elemBase.Province === "Newfoundland and Labrador") elem.Province = eProvince.NewfoundlandAndLabrador;
        else if (elemBase.Province === "Nunavut") elem.Province = eProvince.Nunavut;
        else console.log("UNKNOWN: " + elemBase.Province);

        return elem;
    });

    //Get hash of elected professions
    var electedProfessions: {[key: string]: boolean} = {};
    db.forEach((elem: ElectionResult) => {
        if (elem.Elected) electedProfessions[elem.Occupation.toLowerCase()] = true;
    });//forEach

    //Remove unelected listings of professions that at one point were elected
    db = db.filter((elem: ElectionResult) => {
        if (elem.Elected) return true;

        return !(elem.Occupation.toLowerCase() in electedProfessions);
    });

    console.log("var db = " + JSON.stringify(db, null, 4) + ";");
});
