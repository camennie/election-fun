/// <reference path="jquery/jquery.d.ts" />
/// <reference path="angularjs/angular.d.ts" />
/// <reference path="angularjs/angular-resource.d.ts" />
/// <reference path="angularjs/angular-mocks.d.ts" />
/// <reference path="lodash.d.ts" />


declare var translations: {[key: string]: string};
declare var bindAll: (obj: any) => void;
