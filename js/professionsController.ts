/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./graphView.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />


module Controllers {
    export class ProfessionsController {
        constructor(private UIModel: UIModel.ProfessionsUIModel, private GraphModel: GraphModels.GraphModel,
                    private GraphView: GraphViews.GraphView, private $window: ng.IWindowService,
                    private $scope: ng.IRootScopeService) {
            bindAll(this);
            (<any>$scope).UIModel = UIModel;

            var self = this;
            var resizeHandler = (e: UIEvent) => { self.forceDrawGraph() };
            $window.onresize = resizeHandler;

            //XXX: Unfortunate, but the slider doesn't seem to have a change handler
            $scope.$watch('UIModel.yearRangeStart', self.drawGraph);
            $scope.$watch('UIModel.yearRangeEnd', self.drawGraph);

        }//constructor


        public drawGraph = () => {
            this.GraphModel.updateGraphModel();
            this.GraphView.render();
        }//drawGraph


        public forceDrawGraph = () => {
            this.GraphModel.updateGraphModel();
            this.GraphView.forceRender();
        }//forceDrawGraph
    }//ProfessionsController
}//Controllers


angular.module('ngMainApp')
       .controller('ProfessionsController', ['ProfessionsUIModel', 'ProfessionsGraphModel', 'ProfessionsGraphView',
                    '$window', '$scope', Controllers.ProfessionsController])
