/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />
var eProvince;
(function (eProvince) {
    eProvince[eProvince["Alberta"] = 0] = "Alberta";
    eProvince[eProvince["BritishColumbia"] = 1] = "BritishColumbia";
    eProvince[eProvince["Manitoba"] = 2] = "Manitoba";
    eProvince[eProvince["NewBrunswick"] = 3] = "NewBrunswick";
    eProvince[eProvince["NewfoundlandAndLabrador"] = 4] = "NewfoundlandAndLabrador";
    eProvince[eProvince["NorthwestTerritories"] = 5] = "NorthwestTerritories";
    eProvince[eProvince["NovaScotia"] = 6] = "NovaScotia";
    eProvince[eProvince["Nunavut"] = 7] = "Nunavut";
    eProvince[eProvince["Ontario"] = 8] = "Ontario";
    eProvince[eProvince["PrinceEdwardIsland"] = 9] = "PrinceEdwardIsland";
    eProvince[eProvince["Quebec"] = 10] = "Quebec";
    eProvince[eProvince["Saskatchewan"] = 11] = "Saskatchewan";
    eProvince[eProvince["Yukon"] = 12] = "Yukon";
})(eProvince || (eProvince = {}));

var GraphModels;
(function (GraphModels) {
    var ProfessionCountPair = (function () {
        function ProfessionCountPair() {
        }
        return ProfessionCountPair;
    })();
    GraphModels.ProfessionCountPair = ProfessionCountPair;

    var YearProfessionCountPair = (function () {
        function YearProfessionCountPair() {
        }
        return YearProfessionCountPair;
    })();
    GraphModels.YearProfessionCountPair = YearProfessionCountPair;

    var GraphModel = (function () {
        function GraphModel(UIModel) {
            this.UIModel = UIModel;
            bindAll(this);
        }
        GraphModel.prototype.updateGraphModel = function () {
            var _this = this;
            this.yearToProfessionMapping = this.UIModel.yearToProfessionMapping;
            this.useElected = this.UIModel.useElected;

            //Get db entries that match our current filter
            var filteredDb = db.filter(function (elem) {
                var yearOk = (elem.Year >= _this.UIModel.yearRangeStart) && (elem.Year <= _this.UIModel.yearRangeEnd);

                var electedOk = elem.Elected == _this.UIModel.useElected;

                var provinceOk = false;
                switch (_this.UIModel.provincesSelection.value) {
                    case 0 /* CanadaCombined */:
                        provinceOk = true;
                        break;
                    case 1 /* Alberta */:
                        provinceOk = elem.Province === 0 /* Alberta */;
                        break;
                    case 2 /* BritishColumbia */:
                        provinceOk = elem.Province === 1 /* BritishColumbia */;
                        break;
                    case 3 /* Manitoba */:
                        provinceOk = elem.Province === 2 /* Manitoba */;
                        break;
                    case 4 /* NewBrunswick */:
                        provinceOk = elem.Province === 3 /* NewBrunswick */;
                        break;
                    case 5 /* NewfoundlandAndLabrador */:
                        provinceOk = elem.Province === 4 /* NewfoundlandAndLabrador */;
                        break;
                    case 6 /* NorthwestTerritories */:
                        provinceOk = elem.Province === 5 /* NorthwestTerritories */;
                        break;
                    case 7 /* NovaScotia */:
                        provinceOk = elem.Province === 6 /* NovaScotia */;
                        break;
                    case 8 /* Nunavut */:
                        provinceOk = elem.Province === 7 /* Nunavut */;
                        break;
                    case 9 /* Ontario */:
                        provinceOk = elem.Province === 8 /* Ontario */;
                        break;
                    case 10 /* PrinceEdwardIsland */:
                        provinceOk = elem.Province === 9 /* PrinceEdwardIsland */;
                        break;
                    case 11 /* Quebec */:
                        provinceOk = elem.Province === 10 /* Quebec */;
                        break;
                    case 12 /* Saskatchewan */:
                        provinceOk = elem.Province === 11 /* Saskatchewan */;
                        break;
                    case 13 /* Yukon */:
                        provinceOk = elem.Province === 12 /* Yukon */;
                        break;
                }

                return yearOk && electedOk && provinceOk;
            });

            //Save a bit of time and compute only what we need to display
            if (!this.yearToProfessionMapping) {
                this.computeTopElected(filteredDb);
            } else {
                this.computeTopElectedPerYear(filteredDb);
            }
        };

        GraphModel.prototype.computeTopElectedPerYear = function (filteredDb) {
            var _this = this;
            var self = this;
            this.topElectedPerYear = [];

            this.computeTopElected(filteredDb);

            var topProfessions = {};
            this.topElected.forEach(function (elem) {
                topProfessions[elem.profession.toLowerCase()] = true;
            }); //forEach

            var yearGroups = _.groupBy(filteredDb, function (elem) {
                return elem.Year;
            });

            Object.keys(yearGroups).forEach(function (yearStr) {
                var ers = yearGroups[yearStr];

                ers = ers.filter(function (er) {
                    return er.Occupation.toLowerCase() in topProfessions;
                });

                if (ers.length > 0) {
                    var year = parseInt(yearStr);
                    var pcpsForYear = _this.getProfessionCountPairs(ers);

                    var ypcps = pcpsForYear.map(function (pcp) {
                        return {
                            profession: pcp.profession,
                            count: pcp.count,
                            year: year
                        };
                    });

                    ypcps = ypcps.slice(0, 10);
                    self.topElectedPerYear = self.topElectedPerYear.concat(ypcps);
                }
            });
        };

        GraphModel.prototype.computeTopElected = function (filteredDb) {
            var pcps = this.getProfessionCountPairs(filteredDb);
            this.topElected = pcps.slice(0, 10);
        };

        GraphModel.prototype.getProfessionCountPairs = function (filteredDb) {
            var professionCountMap = {};
            filteredDb.forEach(function (elem) {
                if (!(elem.Occupation in professionCountMap))
                    professionCountMap[elem.Occupation] = 0;
                professionCountMap[elem.Occupation]++;
            }); //forEach

            var electedPairs = [];
            for (var profession in professionCountMap) {
                var pcp = new ProfessionCountPair();
                pcp.profession = profession;
                pcp.count = professionCountMap[profession];

                electedPairs.push(pcp);
            }

            electedPairs.sort(function (pcp1, pcp2) {
                return pcp2.count - pcp1.count;
            });
            return electedPairs;
        };
        return GraphModel;
    })();
    GraphModels.GraphModel = GraphModel;
})(GraphModels || (GraphModels = {}));

angular.module('ngMainApp').service('ProfessionsGraphModel', ['ProfessionsUIModel', GraphModels.GraphModel]);
//# sourceMappingURL=graphModel.js.map
