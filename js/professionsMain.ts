/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />


var bindAll = (obj: any) => {
    _.forIn(obj, (func: any, name: string) => {
        if (_.isFunction(func) === false) return;

        //noinspection JSUnusedAssignment
        func = _.bind(func, obj);
    });
};//bindAll



var safeApply = (scope: ng.IScope, fn: () => void) => {
    //noinspection JSUnresolvedVariable
    var phase = (<any>scope).$root.$$phase;
    if(phase == '$apply' || phase == '$digest') {
        if(fn && _.isFunction(fn)) {
            fn();
        }//if
    } else {
        scope.$apply(fn);
    }//if
};//safeApply


var safeApplyApply = (scope: ng.IScope) => {
    //noinspection JSUnresolvedVariable
    var phase = (<any>scope).$root.$$phase;
    if(phase != '$apply' && phase != '$digest') scope.$apply();
};//safeApply


angular.module('ngMainApp', ['ui-rangeSlider']);
