/// <reference path="./professionsMain.ts" />
/// <reference path="./graphModel.ts" />


declare var renderingPie: boolean;

module UIModel {
    export enum eProvinceSelection {
        CanadaCombined,
        Alberta,
        BritishColumbia,
        Manitoba,
        NewBrunswick,
        NewfoundlandAndLabrador,
        NorthwestTerritories,
        NovaScotia,
        Nunavut,
        Ontario,
        PrinceEdwardIsland,
        Quebec,
        Saskatchewan,
        Yukon
    }//eProvinceSelection


    export class ProvincesSelection {
        public name: string;
        public value: eProvinceSelection;
    }//ProvincesSelection


    export class ProfessionsUIModel {
        public yearRangeStart: number;
        public yearRangeEnd: number;
        public useElected: boolean; //as opposed to unelected
        public provincesSelection: ProvincesSelection;
        public yearToProfessionMapping: boolean;

        public provincesOptions: ProvincesSelection[];
        public minYear: number;
        public maxYear: number;


        constructor(private $rootScope: ng.IRootScopeService) {
            bindAll(this);

            this.useElected = true;

            this.provincesOptions = [
                { name: "All Provinces / Territories", value: eProvinceSelection.CanadaCombined}
                ,{ name: "Alberta", value: eProvinceSelection.Alberta}
                ,{ name: "British Columbia", value: eProvinceSelection.BritishColumbia}
                ,{ name: "Manitoba", value: eProvinceSelection.Manitoba}
                ,{ name: "New Brunswick", value: eProvinceSelection.NewBrunswick}
                ,{ name: "Newfoundland and Labrador", value: eProvinceSelection.NewfoundlandAndLabrador}
                ,{ name: "Northwest Territories", value: eProvinceSelection.NorthwestTerritories}
                ,{ name: "Nova Scotia", value: eProvinceSelection.NovaScotia}
                ,{ name: "Nunavut", value: eProvinceSelection.Nunavut}
                ,{ name: "Ontario", value: eProvinceSelection.Ontario}
                ,{ name: "Prince Edward Island", value: eProvinceSelection.PrinceEdwardIsland}
                ,{ name: "Quebec", value: eProvinceSelection.Quebec}
                ,{ name: "Saskatchewan", value: eProvinceSelection.Saskatchewan}
                ,{ name: "Yukon", value: eProvinceSelection.Yukon}
            ]

            this.provincesSelection = this.provincesOptions[0];
            this.yearToProfessionMapping = !renderingPie;

            this.minYear = _.min(db, (elem: IElectionResult) => { return elem.Year; }).Year;
            this.maxYear = _.max(db, (elem: IElectionResult) => { return elem.Year; }).Year;

            this.yearRangeStart = this.minYear;
            this.yearRangeEnd = this.maxYear;
        }//constructor

    }//ProfessionsUIModel
}//UIModel



angular.module('ngMainApp')
       .service('ProfessionsUIModel', ['$rootScope', UIModel.ProfessionsUIModel]);

