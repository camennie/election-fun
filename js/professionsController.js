/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./graphView.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />
var Controllers;
(function (Controllers) {
    var ProfessionsController = (function () {
        function ProfessionsController(UIModel, GraphModel, GraphView, $window, $scope) {
            var _this = this;
            this.UIModel = UIModel;
            this.GraphModel = GraphModel;
            this.GraphView = GraphView;
            this.$window = $window;
            this.$scope = $scope;
            this.drawGraph = function () {
                _this.GraphModel.updateGraphModel();
                _this.GraphView.render();
            };
            this.forceDrawGraph = function () {
                _this.GraphModel.updateGraphModel();
                _this.GraphView.forceRender();
            };
            bindAll(this);
            $scope.UIModel = UIModel;

            var self = this;
            var resizeHandler = function (e) {
                self.forceDrawGraph();
            };
            $window.onresize = resizeHandler;

            //XXX: Unfortunate, but the slider doesn't seem to have a change handler
            $scope.$watch('UIModel.yearRangeStart', self.drawGraph);
            $scope.$watch('UIModel.yearRangeEnd', self.drawGraph);
        }
        return ProfessionsController;
    })();
    Controllers.ProfessionsController = ProfessionsController;
})(Controllers || (Controllers = {}));

angular.module('ngMainApp').controller('ProfessionsController', [
    'ProfessionsUIModel', 'ProfessionsGraphModel', 'ProfessionsGraphView',
    '$window', '$scope', Controllers.ProfessionsController]);
//# sourceMappingURL=professionsController.js.map
