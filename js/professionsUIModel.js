/// <reference path="./professionsMain.ts" />
/// <reference path="./graphModel.ts" />

var UIModel;
(function (UIModel) {
    (function (eProvinceSelection) {
        eProvinceSelection[eProvinceSelection["CanadaCombined"] = 0] = "CanadaCombined";
        eProvinceSelection[eProvinceSelection["Alberta"] = 1] = "Alberta";
        eProvinceSelection[eProvinceSelection["BritishColumbia"] = 2] = "BritishColumbia";
        eProvinceSelection[eProvinceSelection["Manitoba"] = 3] = "Manitoba";
        eProvinceSelection[eProvinceSelection["NewBrunswick"] = 4] = "NewBrunswick";
        eProvinceSelection[eProvinceSelection["NewfoundlandAndLabrador"] = 5] = "NewfoundlandAndLabrador";
        eProvinceSelection[eProvinceSelection["NorthwestTerritories"] = 6] = "NorthwestTerritories";
        eProvinceSelection[eProvinceSelection["NovaScotia"] = 7] = "NovaScotia";
        eProvinceSelection[eProvinceSelection["Nunavut"] = 8] = "Nunavut";
        eProvinceSelection[eProvinceSelection["Ontario"] = 9] = "Ontario";
        eProvinceSelection[eProvinceSelection["PrinceEdwardIsland"] = 10] = "PrinceEdwardIsland";
        eProvinceSelection[eProvinceSelection["Quebec"] = 11] = "Quebec";
        eProvinceSelection[eProvinceSelection["Saskatchewan"] = 12] = "Saskatchewan";
        eProvinceSelection[eProvinceSelection["Yukon"] = 13] = "Yukon";
    })(UIModel.eProvinceSelection || (UIModel.eProvinceSelection = {}));
    var eProvinceSelection = UIModel.eProvinceSelection;

    var ProvincesSelection = (function () {
        function ProvincesSelection() {
        }
        return ProvincesSelection;
    })();
    UIModel.ProvincesSelection = ProvincesSelection;

    var ProfessionsUIModel = (function () {
        function ProfessionsUIModel($rootScope) {
            this.$rootScope = $rootScope;
            bindAll(this);

            this.useElected = true;

            this.provincesOptions = [
                { name: "All Provinces / Territories", value: 0 /* CanadaCombined */ },
                { name: "Alberta", value: 1 /* Alberta */ },
                { name: "British Columbia", value: 2 /* BritishColumbia */ },
                { name: "Manitoba", value: 3 /* Manitoba */ },
                { name: "New Brunswick", value: 4 /* NewBrunswick */ },
                { name: "Newfoundland and Labrador", value: 5 /* NewfoundlandAndLabrador */ },
                { name: "Northwest Territories", value: 6 /* NorthwestTerritories */ },
                { name: "Nova Scotia", value: 7 /* NovaScotia */ },
                { name: "Nunavut", value: 8 /* Nunavut */ },
                { name: "Ontario", value: 9 /* Ontario */ },
                { name: "Prince Edward Island", value: 10 /* PrinceEdwardIsland */ },
                { name: "Quebec", value: 11 /* Quebec */ },
                { name: "Saskatchewan", value: 12 /* Saskatchewan */ },
                { name: "Yukon", value: 13 /* Yukon */ }
            ];

            this.provincesSelection = this.provincesOptions[0];
            this.yearToProfessionMapping = !renderingPie;

            this.minYear = _.min(db, function (elem) {
                return elem.Year;
            }).Year;
            this.maxYear = _.max(db, function (elem) {
                return elem.Year;
            }).Year;

            this.yearRangeStart = this.minYear;
            this.yearRangeEnd = this.maxYear;
        }
        return ProfessionsUIModel;
    })();
    UIModel.ProfessionsUIModel = ProfessionsUIModel;
})(UIModel || (UIModel = {}));

angular.module('ngMainApp').service('ProfessionsUIModel', ['$rootScope', UIModel.ProfessionsUIModel]);
//# sourceMappingURL=professionsUIModel.js.map
