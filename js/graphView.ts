/// <reference path="./professionsMain.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />
/// <reference path="./typings/d3.d.ts" />


declare var chordMpr: any;
declare var chordRdr: any;


module GraphViews {
    class LabelValuePair {
        label: string;
        value: number;
    }//LabelValuePair


    interface D3DataType<T> {
        data: T;
    }//D3DataType


    export class GraphView {
        private lastGraphModel: GraphModels.GraphModel;

        constructor(private GraphModel: GraphModels.GraphModel) {
            bindAll(this);

            this.lastGraphModel = <GraphModels.GraphModel>{};
        }//constructor


        public render() {
            if (angular.equals(this.lastGraphModel, this.GraphModel)) return;
            this.renderCommon();
        }//render


        public forceRender() {
            this.renderCommon();
        }//render


        public renderCommon() {
            this.lastGraphModel = jQuery.extend(true, {}, this.GraphModel);

            if (!this.GraphModel.yearToProfessionMapping) {
                this.renderPie();
            } else {
                this.renderChord();
            }//if
        }//renderCommon


        private renderPie() {
            var width = $('.span8').innerWidth();
            var radius = width / 4 ;

            //Create new pie
            $("#svgDiv").empty();

            var svg: D3.Selection = d3.select("#svgDiv").append("svg")
                .attr("width", width)
                .attr("height", width / 2)
                .append("g")
                .attr("transform", "translate(" + radius * 2 + "," + radius + ")");

            svg.append("g").attr("class", "slices");
            svg.append("g").attr("class", "labels");
            svg.append("g").attr("class", "lines");

            var pie: D3.Layout.PieLayout = d3.layout.pie()
                .sort(null)
                .value((d: LabelValuePair) => { return d.value; });

            var arc: D3.Svg.Arc = d3.svg.arc()
                .outerRadius(radius * 0.8)
                .innerRadius(radius * 0.4);

            var outerArc: D3.Svg.Arc = d3.svg.arc()
                .innerRadius(radius * 0.9)
                .outerRadius(radius * 0.9);

            var kvpData: LabelValuePair[] = this.GraphModel.topElected.map((elem) => { return { label: elem.profession, value: elem.count }; });
            if (kvpData.length === 0) kvpData.push({ label: "No data for filter", value: 1 });

            this.renderPieSlices(svg, kvpData, pie, arc);
            this.renderPieText(svg, kvpData, pie, outerArc, radius);
            this.renderPiePolylines(svg, kvpData, pie, arc, outerArc, radius);
        }//renderPie


        private static midAngle (d: D3.Layout.ArcDescriptor) {
            return d.startAngle + (d.endAngle - d.startAngle)/2;
        }//midAngle


        private static pieKey (d: D3DataType<LabelValuePair>) {
            return d.data.label;
        }//pieKey


        private renderPieSlices(svg: D3.Selection, kvpData: LabelValuePair[], pie: D3.Layout.PieLayout,
                                arc: D3.Svg.Arc) {
            var colour: D3.Scale.OrdinalScale = d3.scale.ordinal()
                .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

            var pieToolTip = (d: D3DataType<LabelValuePair>) => {
                if (this.GraphModel.useElected) {
                    return d.data.value.toString() + " " + d.data.label + " elected";
                } else {
                    return d.data.value.toString() + " " + d.data.label + " never elected";
                }//if
            }//pieToolTip

            var slice = svg.select(".slices").selectAll("path.slice")
                           .data(pie(kvpData), GraphView.pieKey);

            slice.enter()
                .insert("path")
                .style("fill", (d: D3DataType<LabelValuePair>) => { return colour(d.data.label); })
                .attr("class", "slice")
                .on("mouseover", (d: D3DataType<LabelValuePair>) => {
                    d3.select("#tooltip")
                        .style("visibility", "visible")
                        .html(pieToolTip(d))
                        .style("top", () => { return ((<any>d3.event).pageY)+"px"})
                        .style("left", () => { return ((<any>d3.event).pageX + 20)+"px";})
                })
                .on("mouseout", (d: D3DataType<LabelValuePair>) => { d3.select("#tooltip").style("visibility", "hidden") });

            slice
                .transition().duration(0)
                .attrTween("d", (d: D3DataType<LabelValuePair>) => {
                    (<any>this)._current = (<any>this)._current || d;
                    var interpolate = d3.interpolate((<any>this)._current, d);
                    (<any>this)._current = interpolate(0);
                    return (t: number) => {
                        return arc(interpolate(t));
                    };
                });

            slice.exit()
                 .remove();
        }//renderPieSlices


        private renderPiePolylines(svg: D3.Selection, kvpData: LabelValuePair[], pie: D3.Layout.PieLayout,
                                  arc: D3.Svg.Arc, outerArc: D3.Svg.Arc, radius: number) {
            var polyline = svg.select(".lines").selectAll("polyline")
                              .data(pie(kvpData), GraphView.pieKey);

            polyline.enter()
                    .append("polyline");

            polyline.transition().duration(0)
                    .attrTween("points", (d: D3DataType<LabelValuePair>) => {
                        (<any>this)._current = (<any>this)._current || d;
                        var interpolate = d3.interpolate((<any>this)._current, d);
                        (<any>this)._current = interpolate(0);
                        return (t: number) => {
                            var d2 = interpolate(t);
                            var pos = outerArc.centroid(d2);
                            pos[0] = radius * 0.95 * (GraphView.midAngle(d2) < Math.PI ? 1 : -1);
                            return [arc.centroid(d2), outerArc.centroid(d2), pos];
                        };
                    });

            polyline.exit()
                    .remove();
        }//renderPiePolyline


        private renderPieText(svg: D3.Selection, kvpData: LabelValuePair[], pie: D3.Layout.PieLayout,
                                outerArc: D3.Svg.Arc, radius: number) {
            var text = svg.select(".labels").selectAll("text")
                          .data(pie(kvpData), GraphView.pieKey);

            text.enter()
                .append("text")
                .attr("dy", ".35em")
                .text((d: D3DataType<LabelValuePair>) => {
                    return d.data.label;
                });

            text.transition().duration(0)
                .attrTween("transform", (d: D3DataType<LabelValuePair>) => {
                    (<any>this)._current = (<any>this)._current || d;
                    var interpolate = d3.interpolate((<any>this)._current, d);
                    (<any>this)._current = interpolate(0);
                    return (t: number) => {
                        var d2 = interpolate(t);
                        var pos = outerArc.centroid(d2);
                        pos[0] = radius * (GraphView.midAngle(d2) < Math.PI ? 1 : -1);
                        return "translate("+ pos +")";
                    };
                })

                .styleTween("text-anchor", (d: D3DataType<LabelValuePair>) => {
                    (<any>this)._current = (<any>this)._current || d;
                    var interpolate = d3.interpolate((<any>this)._current, d);
                    (<any>this)._current = interpolate(0);
                    return (t: number) => {
                        var d2 = interpolate(t);
                        return GraphView.midAngle(d2) < Math.PI ? "start":"end";
                    };
                });

            text.exit()
                .remove();
        }//renderPieText


        private renderChord() {
            //Set up data
            var topElectedPerYear: GraphModels.YearProfessionCountPair[] = this.GraphModel.topElectedPerYear;

            if (topElectedPerYear.length === 0) {
                topElectedPerYear.push({
                    profession: "No data for filter",
                    count: 1,
                    year: 0
                });
            }//if

            var mpr = chordMpr(this.GraphModel.topElectedPerYear);

            mpr
                .addValuesToMap('year')
                .addValuesToMap('profession')
                .setFilter(function (row: GraphModels.YearProfessionCountPair, a: any, b: any) {
                    return ((row.profession === a.name && row.year === b.name) ||
                            (row.year === a.name && row.profession === b.name));
                })
                .setAccessor(function (recs: any, a: any, b: any) {
                    if (!recs[0]) return 0;
                    return recs[0].count;
                });

            var matrix: any = mpr.getMatrix();
            var mmap: any = mpr.getMap();


            //Do rendering
            var width = $('.span8').innerWidth();
            var radius = width / 4;

            $("#svgDiv").empty();

            var colours = ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00",
                "#aaabc5", "#8a8900", "#7b0088", "#6b4800", "#005d56", "#d074ff", "#ff8cff",
                "#00abc5", "#008900", "#000088", "#004800", "#000056", "#0074ff", "#448cff",
                "#ffabc5", "#ff8900", "#ff0088", "#ff4800", "#00ff56", "#ff74ff", "#34ff88"];
            colours = colours.concat(colours).concat(colours);

            var fill: D3.Scale.OrdinalScale = d3.scale.ordinal()
                               .domain(colours)
                               .range(colours);

            var chord: D3.Layout.ChordLayout = d3.layout.chord()
                .padding(.02)
                .sortSubgroups(d3.descending)
                .sortChords(d3.descending);

            var arc: D3.Svg.Arc = d3.svg.arc()
                .innerRadius(radius)
                .outerRadius(radius + 20);

            var svg = d3.select("#svgDiv").append("svg:svg")
                .attr("width", width)
                .attr("height", width * 0.75)
                .append("svg:g")
                .attr("id", "circle")
                .attr("transform", "translate(" + radius * 2 + "," + radius * 1.5 + ")");

            svg.append("circle")
               .attr("r", radius + 20);

            var rdr: any = chordRdr(matrix, mmap);
            chord.matrix(matrix);

            var chordPaths: D3.Selection = this.createChordPaths(svg, chord, fill, radius, rdr);
            this.renderChordText(svg, chord, arc, fill, radius, rdr, chordPaths);
        }//renderChord


        private createChordPaths (svg: D3.Selection, chord: D3.Layout.ChordLayout, fill: D3.Scale.OrdinalScale,
                                  radius: number, rdr: any): D3.Selection {
            var self = this;
            var chordTip = (d: any) => {
                var p = d3.format(".2%"), q = d3.format(",.3r")

                if (self.GraphModel.useElected) {
                    return "Elected Info (of top 10 never elected professions):<br/>"
                        + p(d.svalue / d.stotal) + " (" + q(d.svalue) + ") elected in " + d.sname + " election were "
                        + d.tname
                        + (d.sname === d.tname ? "" : ("<br/>while...<br/>"
                            + p(d.tvalue / d.ttotal) + " (" + q(d.tvalue) + ") of "
                            + d.tname + " overall were elected in " + d.sname));
                } else {
                    return "Elected Info (of top 10 professions):<br/>"
                        + p(d.svalue / d.stotal) + " (" + q(d.svalue) + ") unsuccessful candidates in " + d.sname + " election were "
                        + d.tname
                        + (d.sname === d.tname ? "" : ("<br/>while...<br/>"
                            + p(d.tvalue / d.ttotal) + " (" + q(d.tvalue) + ") of "
                            + d.tname + " overall were candidates in " + d.sname));
                }//if
            }//chordTip

            var chordPaths: D3.Selection = svg.selectAll("path.chord")
                .data(chord.chords())
                .enter().append("svg:path")
                .attr("class", "chord")
                .style("stroke", (d: any) => { return d3.rgb(fill(d.target.index)).darker(); })
                .style("fill", (d: any) => { return fill(d.target.index); })
                .attr("d", d3.svg.chord().radius(radius))
                .on("mouseover", (d: any) => {
                    d3.select("#tooltip")
                        .style("visibility", "visible")
                        .html(chordTip(rdr(d)))
                        .style("top", () => { return ((<any>d3.event).pageY - 100)+"px"})
                        .style("left", () => { return ((<any>d3.event).pageX - 100)+"px";})
                })
                .on("mouseout", (d) => { d3.select("#tooltip").style("visibility", "hidden") });

            return chordPaths;
        }//createChordPaths


        private chordMouseOver(chordPaths: D3.Selection, rdr: any, d: any, i: number) {
            var groupTip = (d: any) => {
                var p = d3.format(".1%"), q = d3.format(",.3r")

                var groupYear = parseInt(d.gname)
                if (!_.isNaN(groupYear)) {
                    return "Year: " + d.gname;
                } else {
                    if (this.GraphModel.useElected) {
                        return "Profession Info:<br/>"
                            + d.gname + " : " + q(d.gvalue) + " elected";
                    } else {
                        return "Profession Info:<br/>"
                            + d.gname + " : " + q(d.gvalue) + " candidates";
                    }//if
                }//if
            }//groupTip

            d3.select("#tooltip")
                .style("visibility", "visible")
                .html(groupTip(rdr(d)))
                .style("top", function () { return ((<any>d3.event).pageY - 80)+"px"})
                .style("left", function () { return ((<any>d3.event).pageX - 130)+"px";})

            chordPaths.classed("fade", function(p: any) {
                return p.source.index != i
                    && p.target.index != i;
            });
        }//chordMouseOver


        private renderChordText(svg: D3.Selection, chord: D3.Layout.ChordLayout, arc: D3.Svg.Arc,
                                fill: D3.Scale.OrdinalScale, radius: number, rdr: any,
                                chordPaths: D3.Selection) {
            var g = svg.selectAll("g.group")
                .data(chord.groups())
                .enter().append("svg:g")
                .attr("class", "group")
                .on("mouseover", (d: any, i: number) => { this.chordMouseOver(chordPaths, rdr, d, i); })
                .on("mouseout", (d: any) => { d3.select("#tooltip").style("visibility", "hidden") });

            g.append("svg:path")
                .style("stroke", "black")
                .style("fill", (d: any) => { return fill(d.index); })
                .attr("d", arc);

            g.append("svg:text")
                .each((d: any) => { d.angle = (d.startAngle + d.endAngle) / 2; })
                .attr("dy", ".35em")
                .style("font-family", "helvetica, arial, sans-serif")
                .style("font-size", "10px")
                .style("color", "blue")
                .attr("text-anchor", (d: any) => { return d.angle > Math.PI ? "end" : null; })
                .attr("transform", (d: any) => {
                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                        + "translate(" + (radius + 26) + ")"
                        + (d.angle > Math.PI ? "rotate(180)" : "");
                })
                .text((d: any) => { return rdr(d).gname; });
        }//renderChordText
    }//GraphView

}//GraphViews


angular.module('ngMainApp')
      .service('ProfessionsGraphView', ['ProfessionsGraphModel', GraphViews.GraphView]);
