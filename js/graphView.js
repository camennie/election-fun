/// <reference path="./professionsMain.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />
/// <reference path="./typings/d3.d.ts" />

var GraphViews;
(function (GraphViews) {
    var LabelValuePair = (function () {
        function LabelValuePair() {
        }
        return LabelValuePair;
    })();

    var GraphView = (function () {
        function GraphView(GraphModel) {
            this.GraphModel = GraphModel;
            bindAll(this);

            this.lastGraphModel = {};
        }
        GraphView.prototype.render = function () {
            if (angular.equals(this.lastGraphModel, this.GraphModel))
                return;
            this.renderCommon();
        };

        GraphView.prototype.forceRender = function () {
            this.renderCommon();
        };

        GraphView.prototype.renderCommon = function () {
            this.lastGraphModel = jQuery.extend(true, {}, this.GraphModel);

            if (!this.GraphModel.yearToProfessionMapping) {
                this.renderPie();
            } else {
                this.renderChord();
            }
        };

        GraphView.prototype.renderPie = function () {
            var width = $('.span8').innerWidth();
            var radius = width / 4;

            //Create new pie
            $("#svgDiv").empty();

            var svg = d3.select("#svgDiv").append("svg").attr("width", width).attr("height", width / 2).append("g").attr("transform", "translate(" + radius * 2 + "," + radius + ")");

            svg.append("g").attr("class", "slices");
            svg.append("g").attr("class", "labels");
            svg.append("g").attr("class", "lines");

            var pie = d3.layout.pie().sort(null).value(function (d) {
                return d.value;
            });

            var arc = d3.svg.arc().outerRadius(radius * 0.8).innerRadius(radius * 0.4);

            var outerArc = d3.svg.arc().innerRadius(radius * 0.9).outerRadius(radius * 0.9);

            var kvpData = this.GraphModel.topElected.map(function (elem) {
                return { label: elem.profession, value: elem.count };
            });
            if (kvpData.length === 0)
                kvpData.push({ label: "No data for filter", value: 1 });

            this.renderPieSlices(svg, kvpData, pie, arc);
            this.renderPieText(svg, kvpData, pie, outerArc, radius);
            this.renderPiePolylines(svg, kvpData, pie, arc, outerArc, radius);
        };

        GraphView.midAngle = function (d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        };

        GraphView.pieKey = function (d) {
            return d.data.label;
        };

        GraphView.prototype.renderPieSlices = function (svg, kvpData, pie, arc) {
            var _this = this;
            var colour = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

            var pieToolTip = function (d) {
                if (_this.GraphModel.useElected) {
                    return d.data.value.toString() + " " + d.data.label + " elected";
                } else {
                    return d.data.value.toString() + " " + d.data.label + " never elected";
                }
            };

            var slice = svg.select(".slices").selectAll("path.slice").data(pie(kvpData), GraphView.pieKey);

            slice.enter().insert("path").style("fill", function (d) {
                return colour(d.data.label);
            }).attr("class", "slice").on("mouseover", function (d) {
                d3.select("#tooltip").style("visibility", "visible").html(pieToolTip(d)).style("top", function () {
                    return (d3.event.pageY) + "px";
                }).style("left", function () {
                    return (d3.event.pageX + 20) + "px";
                });
            }).on("mouseout", function (d) {
                d3.select("#tooltip").style("visibility", "hidden");
            });

            slice.transition().duration(0).attrTween("d", function (d) {
                _this._current = _this._current || d;
                var interpolate = d3.interpolate(_this._current, d);
                _this._current = interpolate(0);
                return function (t) {
                    return arc(interpolate(t));
                };
            });

            slice.exit().remove();
        };

        GraphView.prototype.renderPiePolylines = function (svg, kvpData, pie, arc, outerArc, radius) {
            var _this = this;
            var polyline = svg.select(".lines").selectAll("polyline").data(pie(kvpData), GraphView.pieKey);

            polyline.enter().append("polyline");

            polyline.transition().duration(0).attrTween("points", function (d) {
                _this._current = _this._current || d;
                var interpolate = d3.interpolate(_this._current, d);
                _this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = radius * 0.95 * (GraphView.midAngle(d2) < Math.PI ? 1 : -1);
                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                };
            });

            polyline.exit().remove();
        };

        GraphView.prototype.renderPieText = function (svg, kvpData, pie, outerArc, radius) {
            var _this = this;
            var text = svg.select(".labels").selectAll("text").data(pie(kvpData), GraphView.pieKey);

            text.enter().append("text").attr("dy", ".35em").text(function (d) {
                return d.data.label;
            });

            text.transition().duration(0).attrTween("transform", function (d) {
                _this._current = _this._current || d;
                var interpolate = d3.interpolate(_this._current, d);
                _this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = radius * (GraphView.midAngle(d2) < Math.PI ? 1 : -1);
                    return "translate(" + pos + ")";
                };
            }).styleTween("text-anchor", function (d) {
                _this._current = _this._current || d;
                var interpolate = d3.interpolate(_this._current, d);
                _this._current = interpolate(0);
                return function (t) {
                    var d2 = interpolate(t);
                    return GraphView.midAngle(d2) < Math.PI ? "start" : "end";
                };
            });

            text.exit().remove();
        };

        GraphView.prototype.renderChord = function () {
            //Set up data
            var topElectedPerYear = this.GraphModel.topElectedPerYear;

            if (topElectedPerYear.length === 0) {
                topElectedPerYear.push({
                    profession: "No data for filter",
                    count: 1,
                    year: 0
                });
            }

            var mpr = chordMpr(this.GraphModel.topElectedPerYear);

            mpr.addValuesToMap('year').addValuesToMap('profession').setFilter(function (row, a, b) {
                return ((row.profession === a.name && row.year === b.name) || (row.year === a.name && row.profession === b.name));
            }).setAccessor(function (recs, a, b) {
                if (!recs[0])
                    return 0;
                return recs[0].count;
            });

            var matrix = mpr.getMatrix();
            var mmap = mpr.getMap();

            //Do rendering
            var width = $('.span8').innerWidth();
            var radius = width / 4;

            $("#svgDiv").empty();

            var colours = [
                "#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00",
                "#aaabc5", "#8a8900", "#7b0088", "#6b4800", "#005d56", "#d074ff", "#ff8cff",
                "#00abc5", "#008900", "#000088", "#004800", "#000056", "#0074ff", "#448cff",
                "#ffabc5", "#ff8900", "#ff0088", "#ff4800", "#00ff56", "#ff74ff", "#34ff88"];
            colours = colours.concat(colours).concat(colours);

            var fill = d3.scale.ordinal().domain(colours).range(colours);

            var chord = d3.layout.chord().padding(.02).sortSubgroups(d3.descending).sortChords(d3.descending);

            var arc = d3.svg.arc().innerRadius(radius).outerRadius(radius + 20);

            var svg = d3.select("#svgDiv").append("svg:svg").attr("width", width).attr("height", width * 0.75).append("svg:g").attr("id", "circle").attr("transform", "translate(" + radius * 2 + "," + radius * 1.5 + ")");

            svg.append("circle").attr("r", radius + 20);

            var rdr = chordRdr(matrix, mmap);
            chord.matrix(matrix);

            var chordPaths = this.createChordPaths(svg, chord, fill, radius, rdr);
            this.renderChordText(svg, chord, arc, fill, radius, rdr, chordPaths);
        };

        GraphView.prototype.createChordPaths = function (svg, chord, fill, radius, rdr) {
            var self = this;
            var chordTip = function (d) {
                var p = d3.format(".2%"), q = d3.format(",.3r");

                if (self.GraphModel.useElected) {
                    return "Elected Info (of top 10 never elected professions):<br/>" + p(d.svalue / d.stotal) + " (" + q(d.svalue) + ") elected in " + d.sname + " election were " + d.tname + (d.sname === d.tname ? "" : ("<br/>while...<br/>" + p(d.tvalue / d.ttotal) + " (" + q(d.tvalue) + ") of " + d.tname + " overall were elected in " + d.sname));
                } else {
                    return "Elected Info (of top 10 professions):<br/>" + p(d.svalue / d.stotal) + " (" + q(d.svalue) + ") unsuccessful candidates in " + d.sname + " election were " + d.tname + (d.sname === d.tname ? "" : ("<br/>while...<br/>" + p(d.tvalue / d.ttotal) + " (" + q(d.tvalue) + ") of " + d.tname + " overall were candidates in " + d.sname));
                }
            };

            var chordPaths = svg.selectAll("path.chord").data(chord.chords()).enter().append("svg:path").attr("class", "chord").style("stroke", function (d) {
                return d3.rgb(fill(d.target.index)).darker();
            }).style("fill", function (d) {
                return fill(d.target.index);
            }).attr("d", d3.svg.chord().radius(radius)).on("mouseover", function (d) {
                d3.select("#tooltip").style("visibility", "visible").html(chordTip(rdr(d))).style("top", function () {
                    return (d3.event.pageY - 100) + "px";
                }).style("left", function () {
                    return (d3.event.pageX - 100) + "px";
                });
            }).on("mouseout", function (d) {
                d3.select("#tooltip").style("visibility", "hidden");
            });

            return chordPaths;
        };

        GraphView.prototype.chordMouseOver = function (chordPaths, rdr, d, i) {
            var _this = this;
            var groupTip = function (d) {
                var p = d3.format(".1%"), q = d3.format(",.3r");

                var groupYear = parseInt(d.gname);
                if (!_.isNaN(groupYear)) {
                    return "Year: " + d.gname;
                } else {
                    if (_this.GraphModel.useElected) {
                        return "Profession Info:<br/>" + d.gname + " : " + q(d.gvalue) + " elected";
                    } else {
                        return "Profession Info:<br/>" + d.gname + " : " + q(d.gvalue) + " candidates";
                    }
                }
            };

            d3.select("#tooltip").style("visibility", "visible").html(groupTip(rdr(d))).style("top", function () {
                return (d3.event.pageY - 80) + "px";
            }).style("left", function () {
                return (d3.event.pageX - 130) + "px";
            });

            chordPaths.classed("fade", function (p) {
                return p.source.index != i && p.target.index != i;
            });
        };

        GraphView.prototype.renderChordText = function (svg, chord, arc, fill, radius, rdr, chordPaths) {
            var _this = this;
            var g = svg.selectAll("g.group").data(chord.groups()).enter().append("svg:g").attr("class", "group").on("mouseover", function (d, i) {
                _this.chordMouseOver(chordPaths, rdr, d, i);
            }).on("mouseout", function (d) {
                d3.select("#tooltip").style("visibility", "hidden");
            });

            g.append("svg:path").style("stroke", "black").style("fill", function (d) {
                return fill(d.index);
            }).attr("d", arc);

            g.append("svg:text").each(function (d) {
                d.angle = (d.startAngle + d.endAngle) / 2;
            }).attr("dy", ".35em").style("font-family", "helvetica, arial, sans-serif").style("font-size", "10px").style("color", "blue").attr("text-anchor", function (d) {
                return d.angle > Math.PI ? "end" : null;
            }).attr("transform", function (d) {
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" + "translate(" + (radius + 26) + ")" + (d.angle > Math.PI ? "rotate(180)" : "");
            }).text(function (d) {
                return rdr(d).gname;
            });
        };
        return GraphView;
    })();
    GraphViews.GraphView = GraphView;
})(GraphViews || (GraphViews = {}));

angular.module('ngMainApp').service('ProfessionsGraphView', ['ProfessionsGraphModel', GraphViews.GraphView]);
//# sourceMappingURL=graphView.js.map
