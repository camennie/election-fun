/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />
var bindAll = function (obj) {
    _.forIn(obj, function (func, name) {
        if (_.isFunction(func) === false)
            return;

        //noinspection JSUnusedAssignment
        func = _.bind(func, obj);
    });
};

var safeApply = function (scope, fn) {
    //noinspection JSUnresolvedVariable
    var phase = scope.$root.$$phase;
    if (phase == '$apply' || phase == '$digest') {
        if (fn && _.isFunction(fn)) {
            fn();
        }
    } else {
        scope.$apply(fn);
    }
};

var safeApplyApply = function (scope) {
    //noinspection JSUnresolvedVariable
    var phase = scope.$root.$$phase;
    if (phase != '$apply' && phase != '$digest')
        scope.$apply();
};

angular.module('ngMainApp', ['ui-rangeSlider']);
//# sourceMappingURL=professionsMain.js.map
