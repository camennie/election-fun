/// <reference path="./professionsMain.ts" />
/// <reference path="./professionsUIModel.ts" />
/// <reference path="./graphModel.ts" />
/// <reference path="./graphView.ts" />
/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/lodash.d.ts" />
/// <reference path="./typings/angularjs/angular.d.ts" />


declare var renderingPie: boolean;

module Controllers {
    export class HeaderController {
        public renderingPie: boolean;

        constructor(private UIModel: UIModel.ProfessionsUIModel) {
            bindAll(this);

            this.renderingPie = renderingPie;
        }//constructor

    }//TitleController
}//Controllers


angular.module('ngMainApp')
    .controller('HeaderController', ['ProfessionsUIModel', Controllers.HeaderController])
